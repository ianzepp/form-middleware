/**
 * Copyright (c) 2015, roundCorner Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE
 * LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * @author Ian Zepp <ian.zepp@gmail.com>
 */

public without sharing class FormRouter {
    // This is the default URL
    public static final String FORM_BUILDER_URL = '' + $Page.rC_Connect__Campaign_DesignForm;

    // What parameters did we receive?
    public static final Map<String, String> RECV_PARAMS = ApexPages.currentPage().getParameters();
    public static final String RECV_PARAMS_CAMPAIGN_ID = 'id';
    public static final String RECV_PARAMS_BATCH_UPLOAD_ID = 'data';
    public static final String RECV_PARAMS_BATCH_UPLOAD_PUBLIC_TOKEN = 'data';
    public static final String RECV_PARAMS_CONTACT_ID = 'data';
    public static final String RECV_PARAMS_CONTACT_PUBLIC_TOKEN = 'data';
    public static final String RECV_PARAMS_CAMPAIGN_MEMBER_ID = 'data';
    public static final String RECV_PARAMS_CAMPAIGN_MEMBER_PUBLIC_TOKEN = 'data';

    // What parameters should we pass to the form?
    public final Map<String, String> SEND_PARAMS;

    // Default useful records
    public rC_Connect__Batch_Upload__c batchUpload;
    public Account account;
    public Contact contact;
    public Campaign campaign;
    public CampaignMember campaignMember;

    // Nothing special by default in the constructor
    public FormRouter() {}

    // Convenience methods
    public PageReference redirectTo(PageReference pageReference) {
        pageReference.setRedirect(true);
        return pageReference;
    }

    // Convenience methods
    public PageReference redirectTo(Id recordId) {
        return redirecTo(new PageReference('/' + recordId));
    }

    // Process the business rules - The follow logic is provided as an example only
    //

    public PageReference initialize() {
        // If we have a known batch upload record, use it.
        initializeBatchUpload();

        // Try to find the contact based on a URL param token or from a detected batch upload record
        initializeContact();

        // Try to find the campaign based on a known ID or a detected batch upload record
        initializeCampaign();

        // Try to find the contact based on a URL param token or from a detected batch upload record
        initializeCampaignMember();

        // Try to find the account based on a detected contact, campaign member, or batch upload record
        initializeAccount();

        // Process the rules
        return processRouting();
    }

    public void initializeBatchUpload() {
        String reference1 = RECV_PARAMS.get(RECV_PARAMS_BATCH_UPLOAD_ID);
        String reference2 = RECV_PARAMS.get(RECV_PARAMS_BATCH_UPLOAD_PUBLIC_TOKEN);

        try {
            batchUpload = [
                SELECT rC_Connect__Batch_Upload_Account_Matched__c
                     , rC_Connect__Batch_Upload_Contact_1_Matched__c
                     , rC_Connect__Batch_Upload_Contact_2_Matched__c
                     , rC_Connect__Batch_Upload_Campaign_Matched__c
                  FROM rC_Connect__Batch_Upload__c
                 WHERE (Id <> NULL && Id = :reference1)
                    OR (rC_Connect__Public_Token__c <> NULL && rC_Connect__Public_Token__c = :reference2)
                 LIMIT 1
            ];
        } catch (System.Exception problem) {
            batchUpload = new rC_Connect__Batch_Upload__c();
        }
    }
}
